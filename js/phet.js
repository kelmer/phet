/* --------------------- Filter --------------------- */
/**
* Generate the list of subject available
*
* @param object sbj_list list of subject
* @param string mainSubj filter arguments for main subject
* @param string subSubj filter arguments for sub subject
*
* @return array of list resources
*/
function listMaterials(sbj_list, mainSubj=null, subSubj=null) {
	var te = {},
		count = 0;
	mainSubj = (mainSubj != null) ? mainSubj.replace(/[^A-Z0-9]+/ig, '-') : null;
	subSubj = (subSubj != null) ? subSubj.replace(/[^A-Z0-9]+/ig, '-') : null;
	$.each(sbj_list, function(sbj, sbj_info){
		if(mainSubj == null) {
			te[sbj] = sbj_info[1]["link"];
		} else {
			if(typeof(sbj_info[0].category) === "object") {
				sbj_info[0].category.forEach(function(item, index){
					subj = (typeof(item) === "object") ? Object.keys(item)[0] : item;
					if(subj.toLowerCase().replace(/[^A-Z0-9]+/ig, '-') == mainSubj) {
						subj_n = subj.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
						if(typeof(item) === "object"){ 
							sub_cat = item[subj][0].sub_category;
							if(subSubj != null){
								if(typeof(sub_cat) === "object") {
									sub_cat.forEach(function(item, index){
										item = item.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
										if(item == subSubj) {
											// console.log(item + count++ +" => " + subSubj);
											te[sbj] = sbj_info[1].link;
											// te[sbj] = sbj.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
											return false;
										}
									});
								} else {
									sub_cat = sub_cat.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
									if(sub_cat == subSubj) {
										// console.log(sub_cat + count++ +" => " + subSubj);
										te[sbj] = sbj_info[1]["link"];
										// te[sbj] = sbj.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
										return false;
									}
								}
							} else {
								te[sbj] = sbj_info[1]["link"];
								// te[sbj] = sbj.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
								// console.log(count++);
							}
						} else {
							te[sbj] = sbj_info[1]["link"];
							// te[sbj] = sbj.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
							// console.log(count++);
						}
						return false;
					}
				});
			} else {
				if(sbj_info[0].category.toLowerCase().replace(/[^A-Z0-9]+/ig, '-') == mainSubj) {
					te[sbj] = sbj_info[1]["link"];
					// te[sbj] = sbj.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
					// console.log(count++);
				}
			}
		}
	});

	return te;
}
/* --------------------- Filter --------------------- */

/* --------------------- Generate Resources --------------------- */
/**
* Generate the links of PHET
*
* @param array resource_list list of resources
*/
function generateResource(resource_list) {

	var phetSrc = "";
	$.each(phetJson, function(subj, link){
		var tmp = link.replace("\_en\.html", "");
		subjImgName = subj.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
		phetSrc += "<div class='item-list'>";
		// phetSrc += "<span class='caption'><a href='https://phet.colorado.edu/sims/html/" + tmp + "/latest/" + link + "'><img src='img/"+subjImgName+".png' />" + subj + "</a></span>";
		phetSrc += "<span class='caption'><a href='resources/phet/" + link + "''><img src='img/"+subjImgName+".png' />" + subj + "</a></span>";
		phetSrc += "</div>";
	});
	return phetSrc;
}
/* --------------------- Generate Resources --------------------- */
